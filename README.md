# Overview

This charm provides [Gitlab](gitlab.org). GitLab unifies issues, code review, 
CI and CD into a single UI. This charm does not depend on other charms. 

# Usage

To deploy the charm follow these steps:

```
juju deploy gitlab-ce
juju expose gitlab-ce
```

You can then browse to `http://ip-address` to configure the service. The default
credentials are `root`/`password`.


## Scale out Usage

This charm does not scale.

# Configuration

TODO
